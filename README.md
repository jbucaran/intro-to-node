# Getting Started with Node.js 🐢

You already know some HTML, CSS and JavaScript, but did you know JavaScript can also run on a server? The key technology that makes this possible is called [Node.js](https://nodejs.org) and over the next hour we'll try to figure out what all that is about.

## Table of Contents
- [Setup](#setup)
- [npm](#npm)
- [REPL](#repl)
- [Global Objects](#global-objects)
- [Callbacks](#callbacks)
  - [process.nextTick](#processnexttick)
- [Buffers](#buffers)
- [Your first module](#your-first-module)
- [HTTP Hello World](#http-hello-world)
- [Food for Thought](#food-for-thought)

## Setup `2min⏱`

If you still haven't installed Node.js on your system, now it's a good time to do so. The easiest way to get up and running with Node.js is to download its installer from the official website.

- [https://nodejs.org](https://nodejs.org/en/#home-downloadhead)

If you are on macOS and using [Homebrew](https://brew.sh/), you may prefer to install Node.js via the command line.

```
brew install node
```

## npm `2min⏱`

- npm is Node.js package manager.
- Use npm to **install** modules developed by the community.
- Use npm to **publish** your own modules for the benefit of the community.

## REPL `5min⏱`

```
node
console.log(1+1)
var a = 1
console.log(a)
a + 1
```

## Global Objects `5min⏱`

- `process`
  - `process.argv`
- `global`

## Callbacks `5min⏱`

```js
function saySomething(msg) {
  console.log(msg)
}

function callMe(cb, msg) {
  cb(msg)
}

callMe(saySomething, "Call be back!")
```

### `process.nextTick`

```js
console.log(1)

process.nextTick(function() {
  console.log("hello")
})

console.log(2)
```

## Buffers `5min⏱`

```js
var fs = require("fs")
var readFile = fs.readFile

readFile("data", "utf8", function(err, data) {
  console.log(data)
})
```

- You'll use buffers when you read/write binary data to/from files.
  - **Example**: Up/Download data to [S3](https://aws.amazon.com/s3/) (Simple Storage Service) using the AWS SDK.

## Your first module `10min⏱`

```js
// PATH/foo/index.js
module.exports = "foo"
```
```js
// PATH/test/index.js
var myModule = require("../foo")
console.log(myModule) // 0
```
- `module.exports` VS `exports`

## HTTP Hello World `20min⏱`

- **`http`**
  - `createServer(cb)`
      - `cb(res, req)`
  - **Response**
    - `res.write(data)`
    - `res.end()`
  - **Request**
    - `req.url`
- **`fs`**
  - `readFile(path, cb)`
  - `createReadStream(path).pipe(writeableStream)`

```js
var http = require("http")
var fs = require("fs")

http
  .createServer(function(req, res) {
    res.writeHead(200, { "Content-Type": "text/html" })
    res.write("hello <strong>world</strong>")
    res.end()
  })
  .listen(8080)
```

## Food for Thought

- [Node.js /docs](https://nodejs.org/en/docs/)
- [Difference between streams and buffers in Node.js](https://www.quora.com/What-is-the-difference-between-streams-and-buffers-in-JavaScript-Node-js)
- [Daddy, what's a stream?](https://howtonode.org/streams-explained)
